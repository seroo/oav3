import React, {Component} from 'react';
import './App.css';
import AppBarComponent from "./components/main/appbar/AppBarComponent";
import withWidth from '@material-ui/core/withWidth';
import WellCome from "./components/main/welcome/WellCome";
import {Router, Route} from "react-router-dom";
import SignIn from "./components/sub/SignIn"
import createHistory from 'history/createBrowserHistory';
import SignUp from "./components/sub/SignUp";
import Dashboard from "./components/main/Dashboard";
import Profile from "./components/sub/Profile"


// const theme = createMuiTheme({
//     palette: {
//         primary: ,
//     },
//
// })

const style = {
    bodyXs: {
        // paddingRight: '0px',
    },
    bodySm: {
        // paddingRight: '8px',
        marginLeft: '10%',
        marginRight: '10%',
    },
    bodyLg: {
        // paddingRight: '8px',
        marginLeft: '20%',
        marginRight: '20%',
    }
}

const history = createHistory();

class App extends Component {

    render() {
        const {width} = this.props;
        return (
            <Router history={history}>
                    <div className="App" style={width === 'xs' ? style.bodyXs : style.bodySm}>
                        <AppBarComponent/>
                        <Route path="/" exact component={WellCome}/>
                        <Route path="/signin" exact component={SignIn}/>
                        <Route path="/signup" exact component={SignUp}/>
                        <Route path="/dashboard" exact component={Dashboard}/>
                        <Route path="/profile" exact component={Profile}/>
                    </div>
            </Router>
        );
    }
}

export default withWidth()(App);
