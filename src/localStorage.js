export const loadState = () => {
    try {
        const serializedState = sessionStorage.getItem('state');
        if (serializedState === null) {
            return undefined;
        }
        return JSON.parse(serializedState)
    } catch (e) {
        return undefined;
    }
}
export const saveState = (state) => {
    try {
        const serializedState = JSON.stringify(state);
        sessionStorage.setItem('state', serializedState);
    } catch (e) {
        // ignore write errors. maybe log them somewhereelse
    }
}

export const deleteState = () => {
    try {
        if (sessionStorage) {
            sessionStorage.removeItem("state")
        }
    } catch (e) {
        // no need to throw something
    }
}