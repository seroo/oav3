import React, {Component} from 'react';
import {withRouter} from "react-router-dom";
import {connect} from "react-redux";
import withWidth from "@material-ui/core/withWidth";
import {Grid} from "@material-ui/core/";
import LeftSideBar from "./leftsidebar/LeftSideBar"
import ExpenseCard from "./expenseCard/ExpenseCard"
import {getExpenseById, getUserExpenses, deleteExpenseById} from "../../actions/expenseActions"
import ParticipantCard from "./participantCard/ParticipantCard";
import {
    loadParticipantsOfExpense,
    addParticipant,
    deleteParticipant,
    updateParticipant
} from "../../actions/participantActions";
import {clearParticipantsList} from '../../actions/participantActions'
import NewExpense from "./expenseCard/NewExpense";
import Button from "@material-ui/core/es/Button/Button";
import NewParticipantCard from '../main/participantCard/NewParticipantCard'
import EmptyExpense from "./expenseCard/EmptyExpense";
// import * as COLORS from "../colors"
import Divider from "@material-ui/core/es/Divider/Divider";
import Loading from "../sub/Loading";


const style = {
    displayless: {
        display: 'none'
    },
    displayness: {
        display: 'block'
    }
}

class Dashboard extends Component {


    state = {
        selectedExpenseId: this.props.clickedExpense.id,
        addExpense: false,
        participants: this.props.getParticipant.participants || [],
        addParticipant: false,
        expenseLoaded: false,
    }


    selectedId = (id = -1) => {
        if (id === this.props.clickedExpense.id) {
            return false
        }

        if (id !== -1) {
            const {token, isSignedIn} = this.props.auth;
            const {amount} = this.props.clickedExpense;

            this.props.clearParticipantsList();
            this.setState({...this.state, selectedExpenseId: id, addParticipant: false})

            this.props.getExpenseById(isSignedIn, token, id);
            this.props.loadParticipantsOfExpense(isSignedIn, token, id, amount);
        }
        this.setState({
            ...this.state,
            addExpense: false
        })


    }

    handleDeleteExpense = (id) => {

        this.props.deleteExpenseById(
            this.props.auth.isSignedIn,
            this.props.auth.token,
            id
        )
    }

    handleAddNewExpenseSubmitted = () => {
        this.setState({
            ...this.state,
            selectedExpenseId: this.props.clickedExpense.id,
            addExpense: false
        })
    }
    handleAddParticipantCard = () => {
        this.setState({
            ...this.state,
            addParticipant: !this.state.addParticipant
        })
    }
    handleNewExpenseCard = () => {
        this.setState({
            ...this.state,
            addExpense: !this.state.addExpense
        })

    }
    handleAddParticipant = (form) => {
        this.props.addParticipant(
            this.props.auth.isSignedIn,
            this.props.auth.token,
            this.props.clickedExpense.id,
            form,
            this.props.clickedExpense.amount
        )
        this.setState({...this.state, addParticipant: false})
    }
    handleDeleteParticipant = (id) => {
        this.props.deleteParticipant(
            this.props.auth.isSignedIn,
            this.props.auth.token,
            this.props.clickedExpense.id,
            id,
            this.props.clickedExpense.amount
        )
    }
    handleUpdateParticipant = (form) => {
        this.props.updateParticipant(
            this.props.auth.isSignedIn,
            this.props.auth.token,
            this.props.clickedExpense.id,
            form.id,
            form,
            this.props.clickedExpense.amount
        )
        this.props.loadParticipantsOfExpense(
            this.props.auth.isSignedIn,
            this.props.auth.token,
            this.props.clickedExpense.id,
            this.props.clickedExpense.amount,
        )
    }

    componentDidMount() {
        if (this.props.auth.token === null || this.props.auth.isSignedIn === false ||
            this.props.auth.user === null) {
            this.props.history.push("/")
        }

        const {token, isSignedIn} = this.props.auth;
        const {amount, id} = this.props.clickedExpense;
        if (this.props.auth.isSignedIn) {
            this.props.getUserExpenses(isSignedIn, token)
            this.props.loadParticipantsOfExpense(isSignedIn, token, id, amount);
        }
    }

    componentDidUpdate(prevProps, prevState, snapshot) {

        if (prevState.selectedExpenseId !== this.props.clickedExpense.id) {
            this.setState({...this.state, selectedExpenseId: this.props.clickedExpense.id})

        }
        if (prevState.selectedExpenseId === -1 && (prevState.selectedExpenseId !== this.props.clickedExpense.id)) {
            this.props.loadParticipantsOfExpense(this.props.auth.isSignedIn, this.props.auth.token,
                this.props.clickedExpense.id, this.props.clickedExpense.amount);
        }
        if (prevState.participants !== this.props.getParticipant.participants) {
            this.setState({
                ...this.state,
                participants: this.props.getParticipant.participants
            })
        }
    }

    render() {
        const {joints} = this.props;
        let jointCount = 1;
        joints.map((joint) => (
            joint === 1 ? jointCount++ : jointCount + 0
        ))
        if (!this.props.auth.isSignedIn) {
            this.props.history.push("/")
        }


        return (
            <div className={'dasboard participant-card-container'}>
                <Grid className="dashboard" style={{height: "100%"}} container>
                    <Grid item xs={3} sm={2} style={{
                        borderStyle: "none groove none none",
                        borderWidth: "3px",

                    }}>
                        <LeftSideBar selectedId={this.selectedId} addNewExpenseCard={this.handleNewExpenseCard}/>
                    </Grid>
                    <Grid
                        item
                        xs={9}
                        sm={10}
                    >
                        {this.state.addExpense ?
                            <NewExpense
                                handleSubmitted={this.handleAddNewExpenseSubmitted}/> : this.props.expenseCount === 0 ?
                                <EmptyExpense/> :
                                <ExpenseCard clickedExpense={this.props.clickedExpense}
                                             deleteExpense={this.handleDeleteExpense}/>}
                        <Divider style={{
                            marginTop: "16px",
                            marginBottom: "16px"
                        }}/>
                        {this.props.expenses.length > 0 ?
                            <Button style={{
                                marginRight: "auto",
                                marginLeft: "auto",
                                // marginTop: "16px",
                                marginBottom: "16px"
                            }} variant="contained"
                                    onClick={this.handleAddParticipantCard}
                                    className={"SignInButton"}
                            >
                                + add Participant
                            </Button>
                            : null}
                        {this.props.participantsLoading ? <Loading /> : null}
                        <Divider/>
                        <div className={'participant-card-container'}
                             style={this.state.addExpense ? style.displayless : style.displayness}>
                            {!this.state.addExpense && this.state.addParticipant ?
                                <NewParticipantCard addParticipant={this.handleAddParticipant}/> : null}

                            {(this.state.participants !== null && this.state.participants.length > 0) &&

                            this.state.participants.length > 0 ? this.state.participants.map((participant, index) => (
                                <div key={participant.id}><ParticipantCard joint={this.props.joints[index]}
                                                                           jointCount={jointCount}
                                                                           amount={this.props.clickedExpense.amount}
                                                                           participant={participant}
                                                                           updateParticipant={this.handleUpdateParticipant}
                                                                           deleteParticipant={this.handleDeleteParticipant}/><Divider
                                    style={{marginTop: "12px"}}/></div>
                            )) : null
                            }
                        </div>

                    </Grid>
                </Grid>

            </div>
        );

    }
}

const mapDispatchToProps = {
    getUserExpenses,
    getExpenseById,
    loadParticipantsOfExpense,
    clearParticipantsList,
    deleteExpenseById,
    addParticipant,
    deleteParticipant,
    updateParticipant,
}

const mapStateToProps = (state) => {
    return {
        auth: state.auth,
        clickedExpense: state.getExpense.clickedExpense,
        getParticipant: state.getParticipant,
        expenseCount: state.getExpense.expenseList.length,
        joints: state.getParticipant.joints,
        amountOfJoints: state.getParticipant.amountOfJoints,
        expenses: state.getExpense.expenseList,
        participantsLoading: state.getParticipant.loading,
    }
}


export default withRouter(connect(mapStateToProps, mapDispatchToProps)(withWidth()(Dashboard)));