import React from 'react';
import {Link, withRouter} from "react-router-dom";
import {connect} from "react-redux";
import welcome from '../../../welcome.jpg'
import Card from "@material-ui/core/es/Card/Card";
import CardActionArea from "@material-ui/core/es/CardActionArea/CardActionArea";
import CardMedia from "@material-ui/core/es/CardMedia/CardMedia";
import CardContent from "@material-ui/core/es/CardContent/CardContent";
import Typography from "@material-ui/core/es/Typography/Typography";
import CardActions from "@material-ui/core/es/CardActions/CardActions";
import Loading from "../../sub/Loading";


class WellCome extends React.Component {

    handleRedirect = (e) => {
        this.props.history.push("/dashboard")
    }

    componentDidMount() {
        if (this.props.auth.isSignedIn) {
            this.props.history.push("/dashboard")
        }
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.props.auth.isSignedIn) {
            this.props.history.push("/dashboard")
        }
    }

    render() {

        if (this.props.auth.loading) {
            return <Loading/>

        } else {
            return (
                <Card raised={false} >
                    <CardActionArea className={'card-container'} disabled>
                        <CardMedia
                            className={'wellcome-card'}
                            component="img"
                            image={welcome}
                            onClick={() => this.props.history.push("/signin")}
                        />
                        <CardContent>
                            <Typography gutterBottom variant="h5" component="h2">
                                Ortak Harcama
                            </Typography>
                            <Typography component="p">
                                Ortak Harcama, arkadaşlarınızla yaptığınız aktivitelerde onlar adına da yaptığınız
                                ödemelerin takibini kolayca ve ücretsiz yapabileceğiniz bir uygulamadır.
                            </Typography>
                        </CardContent>
                    </CardActionArea>
                    <CardActions className={'card-container'}>
                        <div className={'wellcome-links card-container'}>
                            <div className={'links-style-alt'}>
                                <Link className={'link-style'}
                                      to={"/signin"}>Sign
                                    In</Link>
                            </div>
                            <div className={'links-style-alt'}>
                            <Link to={"/signup"}
                                  style={{marginLeft: "8px"}}
                                  className={'link-style'}>Sign
                                Up</Link>
                            </div>
                        </div>
                    </CardActions>
                </Card>
            );
        }

    }
};
const mapStateToProps = (state) => {
    return {
        auth: state.auth,
    }

}

export default withRouter(connect(mapStateToProps, null)(WellCome));
