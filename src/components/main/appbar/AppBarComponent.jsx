import React, {Component} from 'react';
import AppBar from "@material-ui/core/es/AppBar/AppBar";
import Typography from "@material-ui/core/es/Typography/Typography";
import Toolbar from "@material-ui/core/es/Toolbar/Toolbar";
import AccountCircle from '@material-ui/icons/AccountCircle';
import IconButton from "@material-ui/core/IconButton"
import {withRouter, Link} from 'react-router-dom'
import {connect} from 'react-redux';
import Menu from "@material-ui/core/es/Menu/Menu";
import MenuItem from "@material-ui/core/es/MenuItem/MenuItem";
import {signOut} from "../../../actions/authActions";
import {deleteState} from "../../../localStorage";
import {clearParticipantsList} from '../../../actions/participantActions'

class AppBarComponent extends Component {
    state = {
        anchorEl: null,
    }
    handleClick = event => {
        this.setState({
            ...this.state,
            anchorEl: event.currentTarget
        })
    }
    handleLogout = (event) => {
        const {token} = this.props.auth;
        this.props.signOut(token);

        this.setState({
            ...this.state,
            anchorEl: null
        })
        this.props.clearParticipantsList();
        deleteState();
        this.props.history.push("/")
    }
    handleClose = (event) => {
        this.setState({ anchorEl: null });
    };

    render() {

        const {anchorEl} = this.state;
        const {isSignedIn} = this.props.auth;
        return (
            <div >
                <AppBar position="static" style={{background: "#4C5760"}}>
                    <Toolbar>
                        {/*<IconButton color="inherit" aria-label="Menu">*/}
                            {/*<MenuIcon/>*/}
                        {/*</IconButton>*/}
                        <Link to={"/dashboard"}  className={'link-style'}>
                        <Typography variant="h6" color="inherit">


                            Ortak HarcaMa
                            <Typography variant="overline" color="inherit">


                                Beta


                            </Typography>


                        </Typography>
                        </Link>

                        <div style={{marginLeft: 'auto', marginRight: '0'}}>
                            {isSignedIn ?
                                <div>
                                    <IconButton className={'appbar-icon'} onClick={this.handleClick}> <AccountCircle color="inherit"/>
                                    </IconButton>
                                    <Menu
                                        id="simple-menu"
                                        anchorEl={anchorEl}
                                        open={Boolean(anchorEl)}
                                        onClose={this.handleClose}
                                    >
                                        <MenuItem><Link className={'link-style'} to={"/profile"}>Profile</Link></MenuItem>
                                        <MenuItem onClick={this.handleLogout}>Logout</MenuItem>
                                    </Menu>
                                </div> : <div>
                                    <Link className={'link-style'}
                                          to={"/signin"}>Sign
                                        In</Link>
                                    <Link to={"/signup"}
                                          style={{marginLeft: "8px"}}
                                          className={'link-style'}>Sign
                                        Up</Link>
                                </div>
                            }


                        </div>
                    </Toolbar>
                </AppBar>
            </div>
        );
    }
}

const mapDispatchToProps = {
    signOut,
    clearParticipantsList,
}

const mapStateToProps = (state) => {
    return {
        auth: state.auth,
    }

}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AppBarComponent));