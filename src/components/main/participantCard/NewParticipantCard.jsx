import React, {Component} from 'react';
import {Button, Grid, TextField} from "@material-ui/core";
import FormControlLabel from "@material-ui/core/es/FormControlLabel/FormControlLabel";
import Checkbox from "@material-ui/core/es/Checkbox/Checkbox";

class NewParticipantCard extends Component {
    state = {
        name: '',
        description: '',
        isJoint: 1,
        isPaid: 0,
        amountOfJoint: parseFloat(0),
        amountOfCut: parseFloat(0),
        openDialog: false,
        isChangeAThing: false,
    };




    handleChange = (name) => (event) => {
        this.setState({
            ...this.state,
            [name]: event.target.value,
            isChangeAThing: true,
        });
    };

    clearForm = () => {
        this.setState({
            ...this.state,
            name: '',
            description: '',
            isJoint: 0,
            isPaid: 0,
            amountOfJoint: '',
            amountOfCut: '',
            isChangeAThing: false,
        });
    };

    handleCheckedChange = (name) => (event) => {
        this.setState({
            ...this.state,
            [name]: event.target.checked ? parseInt(1) : parseInt(0),
            isChangeAThing: true,
        });
    };

    handleSubmit = () => {
        this.props.addParticipant(this.state);
        this.setState({...this.state, isChangeAThing: false})
    };

    render() {
        return (
            <Grid direction={"column"} container spacing={8} style={{marginLeft: "8px"}}>
                <Grid item xs={12}>
                    <Grid container className={'column'} justify={"flex-start"} spacing={8} style={{paddingRight: "5%"}}>
                        <TextField
                            fullWidth
                            label="Katilimci adi"
                            placeholder="Name of the Participant"
                            onChange={this.handleChange('name')}
                            value={this.state.name}
                        />
                        <br/>
                        <TextField
                            fullWidth
                            label="Aciklama"
                            onChange={this.handleChange('description')}
                            value={this.state.description}
                            placeholder="Description"
                        />
                        <br/>

                        <FormControlLabel
                            control={
                                <Checkbox
                                    checked={this.state.isJoint === 1}
                                    onChange={this.handleCheckedChange('isJoint')}
                                    className={"Checkbox"}
                                />
                            }
                            label="Harcamaya katılıyor.."
                        />
                        <FormControlLabel
                            control={
                                <Checkbox
                                    checked={this.state.isPaid === parseInt(1)}
                                    onChange={this.handleCheckedChange('isPaid')}
                                    className={"Checkbox"}
                                />
                            }
                            label="Ödeme alındı mı.."
                        />

                        <TextField
                            fullWidth
                            label="indirim"
                            type="number"
                            value={this.state.amountOfCut}
                            onChange={this.handleChange('amountOfCut')}
                            placeholder="0.00"
                        />


                    </Grid>

                </Grid>


                <br/>

                <div style={{marginTop: '12px', marginRight: "auto"}}>
                    <Button
                        disabled={this.state.isChangeAThing === false || this.state.name === ''}
                        variant="contained"
                        className={"SignInButton"}
                        onClick={this.handleSubmit}
                    >
                        Submit
                    </Button>
                    <Button
                        style={{ marginLeft: '16px' }}
                        onClick={this.clearForm}
                        variant="contained"
                        className={"DeleteButton"}
                    >
                        Clear
                    </Button>
                </div>
            </Grid>
        );
    }
}

export default NewParticipantCard;