import React, {Component} from 'react';
import {connect} from 'react-redux';
import {TextField, Grid} from "@material-ui/core";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import Button from "@material-ui/core/Button";
import Loading from "../../sub/Loading";
import DeleteModal from "../../sub/DeleteModal";


class ParticipantCard extends Component {
    state = {
        id: this.props.participant.id || -1,
        name: this.props.participant.name || '',
        description: this.props.participant.description || '',
        isJoint: parseInt(this.props.joint),
        isPaid: this.props.participant.isPaid === "" ? parseInt(0) : parseInt(this.props.participant.isPaid) || parseInt(0),
        // amountOfJoint: this.props.amountOfJoints || parseFloat(0),
        amountOfCut: this.props.participant.amountOfCut,
        expense_id: this.props.participant.expense_id || -1,
        isChangeAThing: false,
        openModal: false,
    };

    componentDidUpdate(prevProps, prevState, snapshot) {

        if (
            prevProps.participant.description !== this.props.participant.description ||
            prevProps.participant.name !== this.props.participant.name ||
            prevProps.participant.amountOfCut !== this.props.participant.amountOfCut ||
            prevState.isJoint !== this.props.joint ||
            prevProps.participant.isPaid !== this.props.participant.isPaid ||
            // prevState.amountOfJoint !== this.props.amountOfJoints ||
            prevProps.participant.id !== this.props.participant.id
        ) {
            this.setState({
                ...this.state,
                id: this.props.participant.id,
                name: this.props.participant.name,
                description: this.props.participant.description,
                isJoint: this.props.joint,
                isPaid: this.props.participant.isPaid === "" ? parseInt(0) : parseInt(this.props.participant.isPaid),
                // amountOfJoint: parseFloat(this.props.amountOfJoints),
                amountOfCut: this.props.participant.amountOfCut === "" ? parseFloat(0) : parseFloat(this.props.participant.amountOfCut),
            })
        }

    }


    handleChange = (name) => (event) => {
        this.setState({
            ...this.state,
            // [name]: (name === "amountOfCut") && event.target.value === "" ? parseFloat(0) : event.target.value,
            [name]: event.target.value,
            isChangeAThing: true,
        });
    };

    handleCheckedChange = (name) => (event) => {
        this.setState({
            ...this.state,
            [name]: event.target.checked ? parseInt(1) : parseInt(0),
            isChangeAThing: true,
        });
    };

    handleDelete = () => {
        this.props.deleteParticipant(this.props.participant.id)

    }
    handleOpenModal = () => {
        this.setState({
            ...this.state,
            openModal: true,
        })
    }
    handleCloseModal = () => {
        this.setState({
            ...this.state,
            openModal: false,
        })
    }

    handleUpdate = () => {
        this.props.updateParticipant(this.state);
        this.setState({
            ...this.state, isChangeAThing: false,
        })
    }

    render() {
        if (this.state.name === "" && this.state.isChangeAThing !== false) {
            this.setState({
                ...this.state,
                isChangeAThing: false
            })
        }
        const {isJoint} = this.state;
        const {jointCount, amount} = this.props
        const amountOfJoint = (amount / jointCount).toFixed(2)
        let amountOfCutToCalculate = 0;

        if (this.state.amountOfCut === "") {
            amountOfCutToCalculate = 0;

        } else {
            amountOfCutToCalculate = this.state.amountOfCut
        }

        const amountOfShare = amountOfJoint - parseFloat(amountOfCutToCalculate).toFixed(2);


        if (this.props.loading) {
            return <Loading/>
        } else
            return (

                <Grid direction={"row"} container spacing={8} style={{marginTop: "16px", height: "100vp !important"}}>
                    <Grid item xs={12}>
                        <Grid container justify={"flex-start"} spacing={8}>
                            <Grid item>
                                <form>
                                    <TextField
                                        style={{margin: '10px 10px', width: "150px"}}
                                        label="Name"
                                        placeholder="Katilimci Adi"
                                        onChange={this.handleChange('name')}
                                        value={this.state.name}
                                        required={true}
                                    />
                                    <TextField
                                        style={{margin: '10px 10px', width: "150px"}}
                                        label="Aciklama"
                                        multiline
                                        onChange={this.handleChange('description')}
                                        value={this.state.description !== null ? this.state.description : ""}
                                        placeholder="Aciklama.."
                                    />
                                    <FormControlLabel
                                        control={
                                            <Checkbox
                                                checked={this.state.isJoint === 1}
                                                onChange={this.handleCheckedChange('isJoint')}
                                                className={"Checkbox"}
                                            />
                                        }
                                        label="Harcamaya katılıyor.." style={{width: "150px"}}
                                    />

                                </form>
                            </Grid>
                            <Grid item className={'participant-card'}>
                                <form>

                                    <FormControlLabel
                                        control={
                                            <Checkbox
                                                checked={this.state.isPaid === 1}
                                                onChange={this.handleCheckedChange('isPaid')}
                                                className={"Checkbox"}
                                            />
                                        }
                                        label="Ödeme alındı mı.." style={{width: "150px"}}
                                    />
                                    <TextField
                                        style={{margin: '10px 10px', width: "150px"}}
                                        label="Uygulanacak indirim"
                                        type="number"

                                        value={isNaN(this.state.amountOfCut) ? "" : this.state.amountOfCut}
                                        onChange={this.handleChange('amountOfCut')}
                                        placeholder="0.00"
                                    />
                                    <TextField
                                        style={{margin: '10px 20px', width: "150px"}}
                                        id="outlined-disabled"
                                        variant="outlined"
                                        label="Harcamadaki Payı"
                                        type="number"
                                        disabled
                                        value={isJoint === 1 ? amountOfShare : 0.00}
                                        placeholder="0.00"
                                    />
                                </form>

                            </Grid>
                        </Grid>
                    </Grid>
                    <div style={{marginTop: '32px', marginLeft: 'auto', marginRight: 'auto', width: "100%"}}>

                        <div>
                            <Button
                                disabled={this.state.isChangeAThing === false}
                                variant="contained"
                                className={"SignInButton"}
                                onClick={this.handleUpdate}
                            >
                                Submit
                            </Button>
                            <Button
                                style={{marginLeft: '16px'}}
                                onClick={this.handleOpenModal}
                                variant="contained"
                                className={"DeleteButton"}
                            >
                                Delete
                            </Button>
                            {this.state.openModal === true ? (
                                <DeleteModal message={'Participant'} delete={this.handleDelete} open={this.state.openModal}
                                             closeModal={this.handleCloseModal}/>
                            ) : null}
                        </div>
                    </div>
                </Grid>
            );
    }
}

const mapStateToProps = (state) => {

    return {
        clickedExpense: state.getExpense.clickedExpense,
        participants: state.getParticipant,
        loading: state.getParticipant.loading,
    }
}

export default connect(mapStateToProps, null)(ParticipantCard);