import React from 'react';
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";

const EmptyExpense = () => {
    return (
        <Paper style={{height: "100%", paddingTop: "30px", display: "flex"}} >
            <Typography style={{marginRight: "auto", marginLeft: "16px"}} variant={"h6"}>{"<= Press button to add an expense"}</Typography>
        </Paper>
    );
};

export default EmptyExpense;
