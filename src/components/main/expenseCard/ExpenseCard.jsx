import React, {Component} from 'react';
import {TextField, Button, Grid} from "@material-ui/core";
import {MenuItem} from "@material-ui/core/";
import {connect} from 'react-redux'
import {updateExpense} from '../../../actions/expenseActions'
import {loadParticipantsOfExpense} from '../../../actions/participantActions'
import DeleteModal from "../../sub/DeleteModal";
import FormControlLabel from "@material-ui/core/es/FormControlLabel/FormControlLabel";
import Checkbox from "@material-ui/core/es/Checkbox/Checkbox";
import Loading from "../../sub/Loading";

class ExpenseCard extends Component {
    state = {
        description: this.props.clickedExpense.description || "",
        place: this.props.clickedExpense.place || "",
        variety: this.props.clickedExpense.variety || "yemek",
        isJoint: parseInt(this.props.clickedExpense.isJoint) || parseInt(1),
        amount: parseFloat(this.props.clickedExpense.amount) || "",
        id: this.props.clickedExpense.id || -1,
        openModal: false,
        isChangeAThing: false,

    };

    // localstate i props a bagli componentlerde bunu kullan
    componentDidUpdate(prevProps) {
        if (
            prevProps.clickedExpense.description !== this.props.clickedExpense.description ||
            prevProps.clickedExpense.place !== this.props.clickedExpense.place ||
            prevProps.clickedExpense.variety !== this.props.clickedExpense.variety ||
            prevProps.clickedExpense.isJoint !== this.props.clickedExpense.isJoint ||
            prevProps.clickedExpense.amount !== this.props.clickedExpense.amount ||
            prevProps.clickedExpense.id !== this.props.clickedExpense.id
        ) {
            this.setState({
                ...this.state,
                description: this.props.clickedExpense.description,
                place: this.props.clickedExpense.place,
                variety: this.props.clickedExpense.variety,
                isJoint: parseInt(this.props.clickedExpense.isJoint),
                amount: parseFloat(this.props.clickedExpense.amount),
                id: this.props.clickedExpense.id,
            })
        }
    }


    handleChange = (name) => (event) => {
        this.setState({
            ...this.state,
            [name]: event.target.value,
            isChangeAThing: true
        });
    };
    handleCheckedChange = (name) => (event) => {
        this.setState({
            ...this.state,
            [name]: event.target.checked === true ? 1 : 0,
            isChangeAThing: true,
        })
    }
    handleSelect = (event) => {
        this.setState({
            ...this.state,
            variety: event.target.value,
            isChangeAThing: true,
        });
    };
    handleOpenModal = () => {
        this.setState({
            ...this.state,
            openModal: true
        })
    }
    handleCloseModal = () => {
        this.setState({
            ...this.state,
            openModal: false
        })
    }
    handleDelete = () => {
        this.props.deleteExpense(this.state.id);
    }
    handleUpdate = () => {
        this.props.updateExpense(
            this.props.isSignedIn,
            this.props.token,
            this.props.clickedExpense.id,
            this.state
        )
        this.props.loadParticipantsOfExpense(
            this.props.isSignedIn,
            this.props.token,
            this.props.clickedExpense.id,
            this.props.clickedExpense.amount,
        )
    }


    render() {
        if ((this.state.amount === "" || isNaN(this.state.amount)) && this.state.isChangeAThing !== false) {
            this.setState({
                ...this.state,
                isChangeAThing: false
            })
        }

        if (this.props.loading) {
            return <Loading></Loading>
        } else
        return (

            <Grid container spacing={8}>
                <Grid item xs={12}>
                    <Grid container spacing={8}>
                        <Grid item>
                            <form>
                                <TextField
                                    style={{margin: '10px 10px'}}
                                    label="Harcama adi"
                                    placeholder="Yemek, içmek vb.."
                                    onChange={this.handleChange('description')}
                                    value={this.state.description}
                                    required={true}
                                />
                                <br/>
                                <TextField
                                    style={{margin: '10px 10px',}}
                                    label="Yer/Mekan"
                                    onChange={this.handleChange('place')}
                                    value={this.state.place !== null ? this.state.place : ""}
                                    placeholder="Nusret/Chia vb.."
                                />
                            </form>

                        </Grid>


                        <form>
                            <TextField
                                style={{margin: '10px 10px'}}
                                label="Variety"
                                select
                                onChange={this.handleSelect}
                                value={this.state.variety}
                            >
                                <MenuItem name={'yemek'} value={'yemek'}>
                                    Yemek
                                </MenuItem>
                                <MenuItem name={'içmek'} value={'içmek'}>
                                    Icmek
                                </MenuItem>
                                <MenuItem name={'etkinlik'} value={'etkinlik'}>
                                    Etkinlik
                                </MenuItem>
                                <MenuItem name={'diger'} value={'diger'}>
                                    Diger
                                </MenuItem>
                            </TextField>
                            <br/>
                            <FormControlLabel
                                control={
                                    <Checkbox
                                        checked={this.state.isJoint === 1 ? true : false}
                                        onChange={this.handleCheckedChange('isJoint')}
                                        className={"Checkbox"}
                                    />
                                }
                                label="Herkes ortak mı?"
                            />
                            <br/>

                            <TextField
                                style={{margin: '10px 10px'}}
                                label="Harcama Tutarı"
                                error
                                required={true}
                                type="number"
                                value={this.state.amount}
                                onChange={this.handleChange('amount')}
                                placeholder="0.00"
                            />

                        </form>
                    </Grid>

                </Grid>


                <br/>

                <div style={{marginTop: '12px', marginRight: "auto", marginLeft: "auto"}}>
                    <Button
                        disabled={this.state.isChangeAThing === false}
                        variant="contained"
                        className={"SignInButton"}
                        onClick={this.handleUpdate}
                    >
                        Submit
                    </Button>
                    <Button
                        style={{marginLeft: '16px'}}
                        onClick={this.handleOpenModal}
                        variant="contained"
                        className={"DeleteButton"}
                    >
                        Delete
                    </Button>
                    {this.state.openModal === true ? (
                        <DeleteModal message={'Expense'} delete={this.handleDelete} open={this.state.openModal}
                                     closeModal={this.handleCloseModal}/>
                    ) : null}


                </div>
            </Grid>

        )

    }
}

const mapStateToProps = (state, ownProps) => {

    return {
        clickedExpense: ownProps.clickedExpense,
        isSignedIn: state.auth.isSignedIn,
        token: state.auth.token,
        loading: !state.getExpense.loaded,
    }
}
const mapDispatchToProps = {
    updateExpense,
    loadParticipantsOfExpense,
}

export default connect(mapStateToProps, mapDispatchToProps)(ExpenseCard);