import React, {Component} from 'react';
import {Button, Grid, TextField, MenuItem} from "@material-ui/core";
import {connect} from "react-redux"
import {saveExpense} from "../../../actions/expenseActions";
import {withRouter} from "react-router-dom";


class NewExpense extends Component {

    state = {
        description: "",
        place: "",
        variety: "yemek",
        isJoint: 1,
        amount: '',
        id: -1,
        openDialog: false,
        isChangeAThing: false,
    };

    handleChange = (name) => (event) => {

        this.setState({
            ...this.state,
            [name]: event.target.value,
            isChangeAThing: true
        });
    };
    handleSelect = (event) => {
        this.setState({
            ...this.state,
            variety: event.target.value,
            isChangeAThing: true,
        });
    };
    handleSave = (event) => {
        const {isSignedIn, token} = this.props.auth;
        this.props.saveExpense(isSignedIn, token, this.state)
        this.props.handleSubmitted();
        this.props.history.push("/dashboard")
    }

    render() {
        return (
            <Grid direction={"column"} container spacing={8} style={{marginLeft: "8px"}}>
                <Grid item xs={12}>
                    <Grid container className={'column'} justify={"flex-start"} spacing={8} style={{paddingRight: "5%"}} >
                        <TextField
                            fullWidth
                            label="Harcama adi"
                            placeholder="Yemek, içmek vb.."
                            onChange={this.handleChange('description')}
                            value={this.state.description}
                        />
                        <br/>
                        <TextField
                            fullWidth
                            label="Yer/Mekan"
                            onChange={this.handleChange('place')}
                            value={this.state.place}
                            placeholder="Nusret/Chia vb.."
                        />
                        <br/>
                            <TextField

                                label="Variety"
                                select
                                onChange={this.handleSelect}
                                value={this.state.variety}
                            >
                                <MenuItem name={'yemek'} value={'yemek'}>
                                    Yemek
                                </MenuItem>
                                <MenuItem name={'içmek'} value={'içmek'}>
                                    Icmek
                                </MenuItem>
                                <MenuItem name={'etkinlik'} value={'etkinlik'}>
                                    Etkinlik
                                </MenuItem>
                                <MenuItem name={'diger'} value={'diger'}>
                                    Diger
                                </MenuItem>
                            </TextField>
                        <br/>

                        <TextField
                            fullWidth
                            label="Harcama Tutarı"
                            error
                            type="number"
                            value={this.state.amount}
                            onChange={this.handleChange('amount')}
                            placeholder="0.00"
                        />


                    </Grid>

                </Grid>


                <br/>

                <div style={{marginTop: '12px', marginRight: "auto"}}>
                    <Button
                        disabled={this.state.isChangeAThing === false || this.state.amount === ''}
                        variant="contained"
                        className={"SignInButton"}
                        onClick={this.handleSave}
                    >
                        Submit
                    </Button>
                    <Button
                        style={{marginLeft: '16px'}}
                        onClick={this.openModal}
                        variant="contained"
                        className={"DeleteButton"}
                    >
                        {/*// TODO BASinca state i sifirlasin*/}
                        Delete
                    </Button>
                    {/*{this.state.openDialog === true ? (*/}
                    {/*<DeleteAlertDialog*/}
                    {/*open={this.state.openDialog}*/}
                    {/*close={this.handleCloseDialog}*/}
                    {/*handleDelete={this.handleDelete}*/}
                    {/*/>*/}
                    {/*) : null}*/}
                </div>
            </Grid>
        );
    }
}

const mapStateToProps = (state) => {
    // console.log(state)
    return {
        auth: state.auth
    }
}
const mapDispatchToProps = {
    saveExpense,
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps) (NewExpense));