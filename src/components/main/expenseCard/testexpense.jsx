import React, {Component} from 'react';
import {TextField, Button, Grid} from "@material-ui/core";
import {MenuItem} from "@material-ui/core/";
import {connect} from 'react-redux'
import {updateExpense} from '../../../actions/expenseActions'
import DeleteModal from "../../sub/DeleteModal";
import FormControlLabel from "@material-ui/core/es/FormControlLabel/FormControlLabel";
import Checkbox from "@material-ui/core/es/Checkbox/Checkbox";

// TODO checkbo ta sıkıntı var anlamadım belki switch koymak lazım
class ExpenseCard extends Component {
    state = {
        description: "",
        place: "",
        variety: "yemek",
        isJoint: parseInt(1),
        amount: 0,
        id: -1,
        openModal: false,
        isChangeAThing: false,

    };


    handleChange = (name) => (event) => {
        this.setState({
            ...this.state,
            [name]: event.target.value,
            isChangeAThing: true
        });
    };
    handleCheckedChange = (name) => (event) => {
        // console.log(event.target.checked)
        this.setState({
            ...this.state,
            [name]: event.target.checked === true ? 1 : 0,
            isChangeAThing: true,
        })
    }
    handleSelect = (event) => {
        this.setState({
            ...this.state,
            variety: event.target.value,
            isChangeAThing: true,
        });
    };
    handleOpenModal = () => {
        this.setState({
            ...this.state,
            openModal: true
        })
    }
    handleCloseModal = () => {
        this.setState({
            ...this.state,
            openModal: false
        })
    }
    handleDelete = () => {
        this.props.deleteExpense(this.state.id);
    }
    handleUpdate = () => {
        this.props.updateExpense(
            this.props.isSignedIn,
            this.props.token,
            this.props.clickedExpense.id,
            this.state
        )
    }
    // componentDidMount() {
    //
    //     if (this.props.clickedExpense && this.props.clickedExpense.id !== -1) {
    //         const {description, amount, isJoint, place, variety, id} = this.props.clickedExpense;
    //         this.setState({
    //             ...this.state,
    //             description: description,
    //             amount: amount,
    //             isJoint: isJoint,
    //             place: place,
    //             variety: variety,
    //             id: id,
    //         })
    //     }
    // }

    static getDerivedStateFromProps(nextProps, prevState) {
        let update = {};
        // console.log(prevState.isJoint)
        // let clickedExpense = {}
        // clickedExpense.amount = nextProps.clickedExpense.amount
        // clickedExpense.description = nextProps.clickedExpense.description
        // clickedExpense.id = nextProps.clickedExpense.id
        // clickedExpense.isJoint = nextProps.clickedExpense.isJoint
        // clickedExpense.place = nextProps.clickedExpense.place
        // clickedExpense.variety = nextProps.clickedExpense.variety
        // console.log(nextProps.clickedExpense.isJoint)

        if (!(Object.keys(nextProps.clickedExpense).length === 0 && nextProps.clickedExpense.constructor === Object)) {
            if (nextProps.clickedExpense.id !== prevState.id) {
                update.id = nextProps.clickedExpense.id;
                return update;
            }
            if (nextProps.clickedExpense.amount !== prevState.amount) {
                update.amount = parseFloat(nextProps.clickedExpense.amount);
                return update;
            }
            if (nextProps.clickedExpense.description !== prevState.description) {
                update.description = nextProps.clickedExpense.description;
                return update;
            }
            if (parseInt(nextProps.clickedExpense.isJoint) !== parseInt(prevState.isJoint)) {
                update.isJoint = parseInt(nextProps.clickedExpense.isJoint);
                return update;
            }
            if (nextProps.clickedExpense.place !== prevState.place) {
                update.place = nextProps.clickedExpense.place;
                return update;
            }
            if (nextProps.clickedExpense.variety !== prevState.variety) {
                update.variety = nextProps.clickedExpense.variety;
                return update;
            }
        } else {
            console.log("BISEY DEGISMEMIS GIBI DAVRANIO")
        }

    }


    render() {
        // console.log(this.state)
        return (


            <Grid direction={"column"} container spacing={8}>
                <Grid item xs={12}>
                    <Grid container className={'column'} justify={"flex-start"} spacing={8}>
                        <Grid item>
                            <form>
                                <TextField
                                    style={{margin: '10px 10px'}}
                                    label="Harcama adi"
                                    placeholder="Yemek, içmek vb.."
                                    onChange={this.handleChange('description')}
                                    value={this.state.description}
                                />
                                <br/>
                                <TextField
                                    style={{margin: '10px 10px',}}
                                    label="Yer/Mekan"
                                    onChange={this.handleChange('place')}
                                    value={this.state.place}
                                    placeholder="Nusret/Chia vb.."
                                />
                            </form>

                        </Grid>

                        <Grid item>

                            <form>
                                <TextField
                                    style={{margin: '10px 10px'}}
                                    label="Variety"
                                    select
                                    onChange={this.handleSelect}
                                    value={this.state.variety}
                                >
                                    <MenuItem name={'yemek'} value={'yemek'}>
                                        Yemek
                                    </MenuItem>
                                    <MenuItem name={'içmek'} value={'içmek'}>
                                        Icmek
                                    </MenuItem>
                                    <MenuItem name={'etkinlik'} value={'etkinlik'}>
                                        Etkinlik
                                    </MenuItem>
                                    <MenuItem name={'diger'} value={'diger'}>
                                        Diger
                                    </MenuItem>
                                </TextField>
                                <br/>
                                <FormControlLabel
                                    control={
                                        <Checkbox
                                            checked={this.state.isJoint === 1 ? true : false}
                                            onChange={this.handleCheckedChange('isJoint')}
                                            color="primary"
                                        />
                                    }
                                    label="Herkes ortak mı?"
                                />
                                <br/>

                                <TextField
                                    style={{margin: '10px 10px'}}
                                    label="Harcama Tutarı"
                                    error
                                    type="number"
                                    value={this.state.amount}
                                    onChange={this.handleChange('amount')}
                                    placeholder="0.00"
                                />
                            </form>
                        </Grid>
                    </Grid>

                </Grid>


                <br/>

                <div style={{marginTop: '12px', marginRight: "auto"}}>
                    <Button
                        disabled={this.state.isChangeAThing === false}
                        variant="contained"
                        color="primary"
                        onClick={this.handleUpdate}
                    >
                        Submit
                    </Button>
                    <Button
                        style={{marginLeft: '16px'}}
                        onClick={this.handleOpenModal}
                        variant="contained"
                        color="secondary"
                    >
                        Delete
                    </Button>
                    {this.state.openModal === true ? (
                        <DeleteModal delete={this.handleDelete} open={this.state.openModal}
                                     closeModal={this.handleCloseModal}/>
                    ) : null}


                </div>
            </Grid>

        )

    }
}

const mapStateToProps = (state) => {

    return {
        // clickedExpense: state.getExpense.clickedExpense,
        isSignedIn: state.auth.isSignedIn,
        token: state.auth.token,
    }
}
const mapDispatchToProps = {
    updateExpense,
}

export default connect(mapStateToProps, mapDispatchToProps)(ExpenseCard);