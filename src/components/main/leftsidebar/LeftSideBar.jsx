import React, {Component} from 'react';
import List from "@material-ui/core/es/List/List";
import ListItem from "@material-ui/core/es/ListItem/ListItem";
import {Button, Typography} from "@material-ui/core";
import Icon from '@material-ui/core/Icon';
import {connect} from 'react-redux'
import {getUserExpenses} from "../../../actions/expenseActions";
import Divider from "@material-ui/core/es/Divider/Divider";
import Loading from '../../sub/Loading'


class LeftSideBar extends Component {

    state = {
        expenses: []
    }

    handleExpenseHeaderClick = (id) => () => {
        this.props.selectedId(id)
    }

    componentDidUpdate(prevProps, prevState, snapshot) {

        if (prevState.expenses !== this.props.expenses) {

            this.setState({
                ...this.state,
                expenses: this.props.expenses
            })

        }

    }

    render() {

        // listede Typography noWrap guzel
        // const expenses = this.props.getExpense && this.props.getExpense.expenseList !== undefined ?
        //     this.props.getExpense.expenseList : [];


        return (
            <div style={{ position: "static", padding: "8px"}}>
                    <List>
                        <Typography variant={"h6"} noWrap={true}> Expenses </Typography>
                        {
                            this.state.expenses ? this.state.expenses.map((expense) => (
                            <div key={expense.id} ><ListItem button onClick={this.handleExpenseHeaderClick(expense.id)}  style={{}}>
                                <Typography noWrap={true} > {expense.description} </Typography>
                            </ListItem>
                            <Divider/></div>
                        )) : <Typography noWrap={true} > Click Add Button Below </Typography>}

                        {this.props.loading ? <Loading > </Loading> : null }

                        <Button style={{marginRight: "auto", marginLeft: "auto", marginTop: "16px"}} variant="contained"
                            onClick={this.props.addNewExpenseCard} className={"SignInButton"}
                        >
                            <Icon color={"inherit"} >add</Icon>
                            {/*<Typography color={"inherit"} > ADD</Typography>*/}
                        </Button>
                    </List>
            </div>
        );
    }
}
const mapDispatchToProps = {
    getUserExpenses,
}
const mapStateToProps = (state) => {
    return {
        auth: state.auth,
        expenses: state.getExpense.expenseList,
        loading: state.getExpense.loading,
    }

}

export default connect(mapStateToProps, mapDispatchToProps)(LeftSideBar);