import React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';

function CircularUnderLoad() {

    return <div style={{marginTop: '8px'}}><CircularProgress disableShrink /> </div> ;
}

export default CircularUnderLoad;