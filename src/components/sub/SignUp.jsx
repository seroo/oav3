import React from 'react';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import {connect} from 'react-redux'
import {register} from '../../actions/authActions'
import {deleteState} from "../../localStorage";
import Loading from "./Loading";

const styles = theme => ({
    main: {
        width: 'auto',
        display: 'block', // Fix IE 11 issue.
        marginLeft: theme.spacing.unit * 3,
        marginRight: theme.spacing.unit * 3,
        [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
            width: 400,
            marginLeft: 'auto',
            marginRight: 'auto',
        },
    },
    paper: {
        marginTop: theme.spacing.unit * 8,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        padding: '8px'
    },
    avatar: {
        margin: theme.spacing.unit,
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: '8px',
    },
    submit: {
        marginTop: theme.spacing.unit * 3,
    },
});

class SignUp extends React.Component {

    state = {
        email: '',
        password: '',
        name: '',
        isChangeAThing: false,
    }

    componentDidMount() {
        if (this.props.auth.isSignedIn) {
            this.props.history.push("/dashboard")
        } else {
            deleteState();
        }

    }

    handleChange = (name) => (event) => {
        this.setState(
            {
                ...this.state,
                [name]: event.target.value,
                isChangeAthing: true,
            }
        )
    }

    handleSubmit = () => (e) => {
        e.preventDefault()
        this.props.register(this.state)
    }


    render() {
        const {email, password, name} = this.state;
        const disabled = !(email !== '' && password !== '' && name !== '')
        if(this.props.auth.isSignedIn) {
            this.props.history.push("/dashboard")
        }
        return (
            <main style={styles.main}>
                <CssBaseline/>
                <Paper style={styles.paper}>
                    <Typography component="h1" variant="h5">
                        Sign Up
                    </Typography>
                    <form style={styles.form}>
                        <div style={{padding: '8px'}}>
                            <FormControl margin="normal" required fullWidth>
                                <InputLabel>Full Name</InputLabel>
                                <Input value={this.state.name} onChange={this.handleChange('name')} id="name"
                                       name="name" autoComplete="name" autoFocus/>
                            </FormControl>
                            <FormControl margin="normal" required fullWidth>
                                <InputLabel htmlFor="email">Email Address</InputLabel>
                                <Input value={this.state.email} onChange={this.handleChange('email')} id="email" name="email" autoComplete="email"/>
                            </FormControl>
                            <FormControl margin="normal" required fullWidth>
                                <InputLabel htmlFor="password">Password</InputLabel>
                                <Input value={this.state.password} onChange={this.handleChange('password')} name="password" type="password" id="password" autoComplete="current-password"/>
                            </FormControl>
                        </div>
                        {this.props.auth.loading ? <Loading /> : null}
                        {this.props.auth.error !== null ?
                            <div><Typography style={{color: "white", backgroundColor: "red"}} component="h1" variant="h5">
                                {this.props.auth.error.response.data.errors.name}
                            </Typography>
                            <Typography style={{color: "white", backgroundColor: "red"}} component="h1" variant="h5">
                                {this.props.auth.error.response.data.errors.email}
                            </Typography>
                            <Typography style={{color: "white", backgroundColor: "red"}} component="h1" variant="h5">
                                {this.props.auth.error.response.data.errors.password}
                            </Typography> </div>
                            : null}
                        <Button
                            type="submit"
                            fullWidth
                            disabled={disabled}
                            variant="contained"
                            className={"SignInButton"}
                            style={styles.submit}
                            onClick={this.handleSubmit()}
                        >
                            Sign UP
                        </Button>
                    </form>
                </Paper>
            </main>
        );
    }
}

const mapDispatchToProps = {
    register,
}

const mapStateToProps = (state) => {

    return {
        auth: state.auth,
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SignUp);