import React from 'react';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import {signIn} from "../../actions/authActions"
import {connect} from "react-redux";
import {withRouter} from "react-router-dom";
import {deleteState} from "../../localStorage";
import Loading from './Loading'

const styles = theme => ({
    main: {
        width: 'auto',
        display: 'block', // Fix IE 11 issue.
        marginLeft: theme.spacing.unit * 3,
        marginRight: theme.spacing.unit * 3,
        [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
            width: 400,
            marginLeft: 'auto',
            marginRight: 'auto',
        },
    },
    paper: {
        marginTop: theme.spacing.unit * 8,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        padding: '8px'
    },
    avatar: {
        margin: theme.spacing.unit,
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: '8px',
    },
    submit: {
        marginTop: theme.spacing.unit * 3,
    },
});

// FIXME Buna ve signup componente validasyon için submit buton üstüne error bilgi bölümü ekle

class SignIn extends React.Component {
    state = {
        email: '',
        password: '',
        remember: false,
        isChangeAThing: false,
    }

    componentDidMount() {
        if (this.props.auth.isSignedIn) {
            this.props.history.push("/dashboard")
        } else {
            deleteState();
        }

    }

    handleChange = (name) => (event) => {
        this.setState(
            {
                ...this.state,
                [name]: event.target.value,
                isChangeAThing: true,
            }
        )
    }
    handleCheckedChange = (name) => (event) => {
        this.setState(
            {
                ...this.state,
                [name]: event.target.checked ? true : false,
            }
        )
    }
    handleSubmit = () => (e) => {
        e.preventDefault();
        const credentials = {
            email: this.state.email,
            password: this.state.password
        }
        this.props.signIn(credentials);
        // this.props.history.push("/")
    }

    render() {
        if(this.props.auth.isSignedIn) {
            this.props.history.push("/dashboard")
        }
        return (

            <main style={styles.main}>
                <CssBaseline/>
                <Paper style={styles.paper}>
                    <Typography component="h1" variant="h5">
                        Sign in
                    </Typography>
                    <form style={styles.form} onSubmit={this.handleSubmit}>
                        <div style={{padding: '8px'}}>
                            <FormControl margin="normal" required fullWidth>
                                <InputLabel htmlFor="email">Email Address</InputLabel>
                                <Input value={this.state.email} onChange={this.handleChange('email')} id="email"
                                       name="email" autoComplete="email" autoFocus/>
                            </FormControl>
                            <FormControl margin="normal" required fullWidth>
                                <InputLabel htmlFor="password">Password</InputLabel>
                                <Input value={this.state.password} onChange={this.handleChange('password')}
                                       name="password" type="password" id="password" autoComplete="current-password"/>
                            </FormControl>
                        </div>
                        <FormControlLabel
                            control={<Checkbox onChange={this.handleCheckedChange('remember')}
                                               checked={this.state.remember} value="remember" color="primary"/>}
                            label="Remember me"
                        />
                        {this.props.auth.loading ? <Loading /> : null}
                        {this.props.auth.error !== null ?
                            <Typography style={{color: "white", backgroundColor: "red"}} component="h1" variant="h5">
                            {this.props.auth.error.response.data.message}
                        </Typography> : null}
                        <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                            className={"SignInButton"}
                            style={styles.submit}
                            onClick={this.handleSubmit()}
                            disabled={this.state.isChangeAThing === false}
                        >
                            Sign in
                        </Button>
                    </form>
                </Paper>
            </main>
        );
    }
}

const mapDispatchToProps = {
    signIn,
}
const mapStateToProps = (state) => {
    return {
        auth: state.auth,
    }
}


export default withRouter(connect(mapStateToProps, mapDispatchToProps)(SignIn));