import React, {Component} from 'react';
import {connect} from "react-redux";
import {updateProfile, deleteUserAccount} from '../../actions/authActions'
import Paper from "@material-ui/core/es/Paper/Paper";
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from "@material-ui/core/es/Typography/Typography";
import FormControl from "@material-ui/core/es/FormControl/FormControl";
import InputLabel from "@material-ui/core/es/InputLabel/InputLabel";
import Input from "@material-ui/core/es/Input/Input";
import Button from "@material-ui/core/es/Button/Button";
import {TextField} from "@material-ui/core";
import {unstable_Box as Box} from '@material-ui/core/Box';
import Loading from "./Loading";
import DeleteModal from "./DeleteModal";
import {deleteState} from "../../localStorage";


const styles = theme => ({
    main: {
        width: 'auto',
        display: 'block', // Fix IE 11 issue.
        marginLeft: theme.spacing.unit * 3,
        marginRight: theme.spacing.unit * 3,
        [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
            width: 400,
            marginLeft: 'auto',
            marginRight: 'auto',
        },
    },
    paper: {
        marginTop: theme.spacing.unit * 8,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        padding: '8px'
    },
    avatar: {
        margin: theme.spacing.unit,
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: '8px',
    },
    submit: {
        marginTop: theme.spacing.unit * 3,
    },
});

// FIXME BUG VAR PROFILE GUNCELLESEK BILE BI DAHA PROFILE SAYFASINA GELINCE ESKI BILGILER YUKLENİYOR

class Profile extends Component {

    state = {
        email: '',
        password: '',
        newPassword: '',
        newPassword2: '',
        name: '',
        error: -1,
        isChangeAThing: false,
        openModal: false,
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (!this.props.auth.isSignedIn) {
            return this.props.history.push("/")
        }
        if (prevProps.auth.user.email !== this.props.auth.user.email || prevProps.auth.user.name !== this.props.auth.user.name) {
            this.setState({
                ...this.state,
                email: this.props.auth.user.email,
                name: this.props.auth.user.name
            })
        }


    }

    componentDidMount() {
        if (!this.props.auth.isSignedIn) {
            return this.props.history.push("/")
        }
        if (this.props.auth.user.email && this.props.auth.user.email.length > 0) {
            this.setState({
                ...this.state,
                email: this.props.auth.user.email,
                name: this.props.auth.user.name
            })
        }
    }

    errorMessages = [
        'ZORUNLU ALANLARI DOLDURUN',
        'HATALI ESKI SIFRE',
        'YENI SIFRELER UYUMSUZ'
    ]

    handleChange = (name) => (event) => {
        this.setState(
            {
                ...this.state,
                [name]: event.target.value,
                isChangeAThing: true,
            }
        )
    }

    handleSubmit = () => (e) => {
        e.preventDefault()
        this.setState({...this.state, isChangeAThing: false})
        this.props.updateProfile(this.state, this.props.auth.token)
    }

    handleOpenModal = () => {
        this.setState({
            ...this.state,
            openModal: true,
        })
    }
    handleCloseModal = () => {
        this.setState({
            ...this.state,
            openModal: false
        })
    }
    handleDeleteUser = () => {
        const token = this.props.auth.token
        this.props.deleteUserAccount(token)
        deleteState()
    }

    render() {

        return (
            <main style={styles.main}>
                <CssBaseline/>
                <Paper style={styles.paper}>
                    <Typography component="h1" variant="h5">
                        Profile
                    </Typography>
                    <form style={styles.form}>
                        <div style={{padding: '8px'}}>
                            <FormControl margin="normal" required fullWidth>
                                <InputLabel>Full Name</InputLabel>
                                <Input value={this.state.name} onChange={this.handleChange('name')} id="name"
                                       name="name" autoComplete="name" autoFocus error={this.state.name.length < 1}/>
                            </FormControl>
                            <FormControl margin="normal" required fullWidth>
                                <InputLabel htmlFor="email">Email Address</InputLabel>
                                <Input value={this.state.email} onChange={this.handleChange('email')} id="email"
                                       name="email" error={this.state.email.length < 1} autoComplete="email"/>
                            </FormControl>
                            <FormControl margin="normal" required fullWidth>
                                <InputLabel htmlFor="password">Old Password</InputLabel>
                                <Input value={this.state.password} onChange={this.handleChange('password')}
                                       name="password" type="password" id="password" autoComplete="current-password"/>
                            </FormControl>
                            <FormControl margin="normal" required fullWidth>
                                <InputLabel htmlFor="password">New Password</InputLabel>
                                <Input value={this.state.newPassword} onChange={this.handleChange('newPassword')}
                                       name="newPassword" type="password" id="newPassword"
                                       autoComplete="current-password"/>
                            </FormControl>
                            <FormControl margin="normal" required fullWidth>
                                <InputLabel htmlFor="password">New Password (again)</InputLabel>
                                <Input value={this.state.newPassword2} onChange={this.handleChange('newPassword2')}
                                       name="newPassword2" type="password" id="newPassword2"
                                       autoComplete="current-password"

                                />
                            </FormControl>
                            <div className={'deleteUserButtonContainer'}>
                                <Button
                                    color="secondary" size={'small'}
                                    onClick={this.handleOpenModal}

                                >
                                    <Typography variant={'subtitle2'}> Delete User </Typography>
                                </Button>
                                {this.state.openModal === true ? (
                                    <DeleteModal message={'User'} delete={this.handleDeleteUser}
                                                 open={this.state.openModal}
                                                 closeModal={this.handleCloseModal}/>
                                ) : null}
                            </div>

                            <Box display={this.state.error === -1 ? 'none' : 'block'}>
                                <TextField
                                    value={this.state.error > -1 ? this.errorMessages[this.state.error] : "No error"}
                                    fullWidth
                                    disabled
                                    error
                                    id="outlined-error"
                                    label="Error"
                                    margin="normal"
                                    variant="outlined"
                                />
                            </Box>
                        </div>
                        {this.props.loading ? <Loading/> : null}
                        {this.props.auth.error !== null ?
                            <div><Typography style={{color: "white", backgroundColor: "red"}} component="h1"
                                             variant="h5">
                                {this.props.auth.error.response.data.errors.name}
                            </Typography>
                                <Typography style={{color: "white", backgroundColor: "red"}} component="h1"
                                            variant="h5">
                                    {this.props.auth.error.response.data.errors.email}
                                </Typography>
                                <Typography style={{color: "white", backgroundColor: "red"}} component="h1"
                                            variant="h5">
                                    {this.props.auth.error.response.data.errors.password}
                                </Typography></div>
                            : null}
                        <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                            className={"SignInButton"}
                            style={styles.submit}
                            onClick={this.handleSubmit()}
                            disabled={!(this.state.isChangeAThing)}
                        >
                            Update Profile
                        </Button>
                    </form>
                </Paper>
            </main>
        );
    }
}


const mapStateToProps = (state) => {

    return {
        auth: state.auth,
        loading: state.auth.loading,
    }
}

const mapDispatchToProps = {
    updateProfile,
    deleteUserAccount,
}


export default connect(mapStateToProps, mapDispatchToProps)(Profile);
