import React, {Component} from 'react';
import Button from "@material-ui/core/es/Button/Button";
import Dialog from "@material-ui/core/es/Dialog/Dialog";
import DialogTitle from "@material-ui/core/es/DialogTitle/DialogTitle";
import DialogActions from "@material-ui/core/es/DialogActions/DialogActions";

class DeleteModal extends Component {
    state = {
        openModal: this.props.open || false,
    };

    handleClose = () => {
        this.props.closeModal();
    };

    render() {
        return (
            <div>
                <Dialog
                    open={this.state.openModal}
                    onClose={this.handleClose}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                >
                    <DialogTitle id="alert-dialog-title">{"Are U Sure to Delete " + this.props.message}</DialogTitle>
                    <DialogActions>
                        <Button onClick={this.handleClose} color="primary">
                            Nope
                        </Button>
                        <Button onClick={() => {this.props.delete();this.handleClose()}} color="primary" autoFocus>
                            Delete
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        );
    }
}

export default DeleteModal;