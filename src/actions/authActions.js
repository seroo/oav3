import {
    SIGN_IN_START,
    SIGN_IN_DONE,
    SIGN_IN_ERROR,
    SIGN_OUT_START,
    SIGN_OUT_DONE,
    SIGN_OUT_ERROR,
    DELETE_AUTH_STATE,
    REGISTER_START,
    REGISTER_DONE,
    REGISTER_ERROR,
    UPDATE_PROFILE_START,
    UPDATE_PROFILE_DONE,
    UPDATE_PROFILE_ERROR,
    GET_USER_DETAIL_DONE,
    GET_USER_DETAIL_ERROR,
    DELETE_USER_ACCOUNT_START,
    DELETE_USER_ACCOUNT_DONE,
    DELETE_USER_ACCOUNT_ERROR,
    localURL, GET_USER_DETAIL_START,
} from "./actionConstants";
import axios from 'axios'

export const signIn = (credentials) => {
    return async (dispatch) => {
        dispatch({
            type: SIGN_IN_START
        })
        await axios
            .post(
                `${localURL}/api/login`, credentials
            )
            .then(({data}) => {
                if (data.success === true) {
                    dispatch({
                        type: SIGN_IN_DONE,
                        payload: data
                    })
                }
            })
            .catch((error) => {
                console.log(error)
                dispatch({
                    type: SIGN_IN_ERROR,
                    payload: error
                })
            })
    } // ilk dispatch
}

export const signOut = (token) => {
    return async (dispatch) => {

        dispatch({
            type: SIGN_OUT_START
        })
        await axios
            .get(
                `${localURL}/api/logout?token=${token}`
            )
            .then(({data}) => {
                if (data.success === true) {

                    dispatch({
                        type: SIGN_OUT_DONE,
                        payload: data
                    })
                }
            })
            .catch((error) => {
                dispatch({
                    type: SIGN_OUT_ERROR,
                    payload: error
                })
            })
    } // ilk dispatch

}

export const register = (credentials) => {

    return async (dispatch) => {
        dispatch({
            type: REGISTER_START
        })

        await axios
            .post(`${localURL}/api/register`, credentials
            )
            .then(({data}) => {
                if (data.success === true) {
                    dispatch({
                        type: REGISTER_DONE,
                        payload: data
                    })
                }
            })
            .catch((error) => {
                dispatch({
                    type: REGISTER_ERROR,
                    payload: error
                })
            })
    }
}

export const updateProfile = (form, token) => {

    return async (dispatch) => {
        dispatch({
            type: UPDATE_PROFILE_START
        })

        await axios
            .post(`${localURL}/api/user/update?token=${token}`, form)
            .then(({data, message}) => {
                // if (data.message !== null || data.message !== 'undefined' || data.message.length > 0) {
                //     dispatch({
                //         type: UPDATE_PROFILE_ERROR,
                //         payload: message
                //     })
                // } // BUNA FARKLI BISEY DUSUNMEK LAZIM
                if (data.success === true) {
                    dispatch({
                        type: UPDATE_PROFILE_DONE,
                        payload: data
                    })
                    dispatch(getUserDetail(token))
                }
            })
            .catch((error) => {
                dispatch({
                    type: UPDATE_PROFILE_ERROR,
                    payload: error
                })
            })
    }
}

export const getUserDetail = (token) => {

    return async (dispatch) => {
        dispatch({
            type: GET_USER_DETAIL_START,
        })
        await axios
            .get(`${localURL}/api/user?token=${token}`)
            .then(({data}) => {
                if (data.message === 'success') {
                    dispatch({
                        type: GET_USER_DETAIL_DONE,
                        payload: data
                    })
                }
        }).catch((error) => {
            dispatch({
                type: GET_USER_DETAIL_ERROR,
                payload: error
            })
            })

    }
}

export const deleteAuthState = () => {
    return {type: DELETE_AUTH_STATE}
}

export const deleteUserAccount = (token) => {
    return async (dispatch) => {
        console.log("async dispatch")
        dispatch({
            type: DELETE_USER_ACCOUNT_START,
        })
        await axios
            .delete(`${localURL}/api/user/delete?token=${token}`)
            .then(({data}) => {
                console.log(data)
                if (data.success === true) {
                    dispatch({
                        type: DELETE_USER_ACCOUNT_DONE
                    })
                    dispatch({
                        type: DELETE_AUTH_STATE
                    });
                }
            }).catch((error) => {
                console.log("DELETE ACCOUNT ERROR")
                dispatch({
                    type: DELETE_USER_ACCOUNT_ERROR,
                    payload: error
                })
            })
    }
}
// export const deleteUserAccount = (token) => {
//     console.log(token)
//     return async (dispatch) => {
//         console.log("async dispatch")
//         dispatch({
//             type: DELETE_USER_ACCOUNT_START,
//         })
//         await axios
//             .delete(`${localURL}/api/user/delete`, token.toJSON())
//             .then(({data}) => {
//                 console.log(data)
//                 // if (data.success === true) {
//                 //     dispatch({
//                 //         type: DELETE_USER_ACCOUNT_DONE
//                 //     })
//                 //     dispatch(deleteAuthState());
//                 // }
//             }).catch((error) => {
//                 console.log("DELETE ACCOUNT ERROR")
//                 dispatch({
//                     type: DELETE_USER_ACCOUNT_ERROR,
//                     payload: error
//                 })
//             })
//     }
// }

// export const deleteUserAccount = (token) => {
//
//     return async (dispatch) => {
//         dispatch({
//             type: DELETE_USER_ACCOUNT_START,
//         })
//         await axios
//             .get(`${localURL}/api/user/delete`, token)
//             .then(({data}) => {
//                 if (data.message === 'success') {
//                     dispatch({
//                         type: GET_USER_DETAIL_DONE,
//                         payload: data
//                     })
//                 }
//             }).catch((error) => {
//                 dispatch({
//                     type: GET_USER_DETAIL_ERROR,
//                     payload: error
//                 })
//             })
//
//     }
// }