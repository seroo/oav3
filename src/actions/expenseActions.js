import {CLEAR_PARTICIPANTS_LIST, DELETE_AUTH_STATE, GET_EXPENSE_LIST_START} from "./actionConstants"
import {GET_EXPENSE_LIST_DONE} from "./actionConstants"
import {GET_EXPENSE_LIST_ERROR} from "./actionConstants"
import {
    GET_EXPENSE_BY_ID_START,
    GET_EXPENSE_BY_ID_DONE,
    GET_EXPENSE_BY_ID_ERROR,
    SAVE_EXPENSE_START,
    SAVE_EXPENSE_DONE,
    SAVE_EXPENSE_ERROR,
    DELETE_EXPENSE_START,
    DELETE_EXPENSE_DONE,
    DELETE_EXPENSE_ERROR,
    UPDATE_EXPENSE_START,
    UPDATE_EXPENSE_DONE,
    UPDATE_EXPENSE_ERROR,

} from "./actionConstants";
import {localURL} from "./actionConstants";
import axios from 'axios'
import {deleteState} from "../../src/localStorage"
import formUrlEncode from 'form-urlencoded';

export const getUserExpenses = (isSignedIn, token) => {

    if (!isSignedIn) {
        return dispatch => ({
            type: GET_EXPENSE_LIST_ERROR, payload: "invalid user"
        })
    }

    return async (dispatch) => {
        dispatch({
            type: GET_EXPENSE_LIST_START
        });
        await axios
            .get(`${localURL}/api/expenses?token=${token}`)
            .then(({data}) => {
                if (data.length > 0) {
                    dispatch({
                        type: GET_EXPENSE_LIST_DONE,
                        payload: data
                    });
                } else {
                    dispatch({
                        type: GET_EXPENSE_LIST_DONE,
                        payload: []
                    });
                }
            })
            .catch((error) => {

                dispatch({
                    type: GET_EXPENSE_LIST_ERROR,
                    payload: error
                })
                if (error.response.data.message === "Token has expired") {
                    dispatch({
                        type: CLEAR_PARTICIPANTS_LIST
                    })
                    deleteState();
                    dispatch({type: DELETE_AUTH_STATE})
                }
            })
    }

}

export const getExpenseById = (isSignedIn, token, id) => {

    if (!isSignedIn) {
        return dispatch => ({
            type: GET_EXPENSE_LIST_ERROR, payload: "invalid user"
        })
    }

    if (id < 1) {
        return {type: GET_EXPENSE_BY_ID_ERROR, payload: "hatali expense id"};
    } else {

        return (dispatch) => {
            dispatch({type: GET_EXPENSE_BY_ID_START});
            axios
                .get(
                    `${localURL}/api/expense/${id}?token=${token}`)
                .then(({data}) => {
                    if (data) {
                        dispatch({type: GET_EXPENSE_BY_ID_DONE, payload: data});
                    }
                })
                .catch((error) => {
                    dispatch({type: GET_EXPENSE_BY_ID_ERROR, payload: error});
                    if (error.response.data.message === "Token has expired") {
                        dispatch({
                            type: CLEAR_PARTICIPANTS_LIST
                        })
                        deleteState();
                        dispatch({type: DELETE_AUTH_STATE})
                    }
                })
        }
    }
}

export const saveExpense = (isSignedIn, token, form) => {

    if (form.place.length === 0) {
        form.place = null
    }

    if (!isSignedIn) {
        return dispatch => ({
            type: GET_EXPENSE_LIST_ERROR, payload: "invalid user"
        })
    }

    form.token = token;
    return async (dispatch) => {
        dispatch({
            type: SAVE_EXPENSE_START
        });
        await axios
            .post(`${localURL}/api/expense`, form)
            .then(({data}) => {

                if (data.success === true) {
                    dispatch({type: SAVE_EXPENSE_DONE, payload: data.expense})
                    dispatch(getUserExpenses(isSignedIn, token));
                    dispatch({
                        type: CLEAR_PARTICIPANTS_LIST
                    })
                }


            })
            .catch((error) => {
                dispatch({type: SAVE_EXPENSE_ERROR, payload: error})
                if (error.response.data.message === "Token has expired") {
                    dispatch({
                        type: CLEAR_PARTICIPANTS_LIST
                    })
                    deleteState();
                    dispatch({type: DELETE_AUTH_STATE})
                }

                // deleteState();
                // dispatch({
                //     type: SIGN_OUT_START
                // })
                // dispatch({type: DELETE_AUTH_STATE})
                // deleteState();
                // dispatch({
                //     type: GET_EXPENSE_LIST_ERROR,
                //     payload: error
                // })
            })
    }

}

export const deleteExpenseById = (isSignedIn, token, id) => {
    if (!isSignedIn) {
        return dispatch => ({
            type: DELETE_EXPENSE_ERROR, payload: "invalid user"
        })
    }

    return async (dispatch) => {
        dispatch({
            type: DELETE_EXPENSE_START
        });
        await axios
            .delete(`${localURL}/api/expense/${id}?token=${token}`)
            .then(({data}) => {

                if (data.success === true) {
                    dispatch({type: DELETE_EXPENSE_DONE, payload: data.expense})
                    dispatch(getUserExpenses(isSignedIn, token));
                }

            })
            .catch((error) => {
                dispatch({type: DELETE_EXPENSE_ERROR, payload: error})
                if (error.response.data.message === "Token has expired") {
                    dispatch({
                        type: CLEAR_PARTICIPANTS_LIST
                    })
                    deleteState();
                    dispatch({type: DELETE_AUTH_STATE})
                }

                // deleteState();
                // dispatch({
                //     type: SIGN_OUT_START
                // })
                // dispatch({type: DELETE_AUTH_STATE})
                // deleteState();
                // dispatch({
                //     type: GET_EXPENSE_LIST_ERROR,
                //     payload: error
                // })
            })
    }


}
export const updateExpense = (isSignedIn, token, id, form) => {
    const urlEncodedForm = formUrlEncode(form)
    if (!isSignedIn) {
        return dispatch => ({
            type: UPDATE_EXPENSE_ERROR, payload: "invalid user"
        })
    }
    return async (dispatch) => {
        dispatch({
            type: UPDATE_EXPENSE_START
        })
        await axios
            .put(`${localURL}/api/expense/${id}?token=${token}`, urlEncodedForm)
            .then(({data}) => {
                if (data.success === true) {
                    dispatch({type: UPDATE_EXPENSE_DONE, payload: data.expense})
                    dispatch(getUserExpenses(isSignedIn, token))
                }
            })
            .catch((error) => {
                dispatch({type: UPDATE_EXPENSE_ERROR, payload: error})
                if (error.response.data.message === "Token has expired") {
                    dispatch({
                        type: CLEAR_PARTICIPANTS_LIST
                    })
                    deleteState();
                    dispatch({type: DELETE_AUTH_STATE})
                }

            })
    }
}

export const changeClickedExpense = () => {

    // sonra dusunucem bisiyle henuz gerek yok gibi

}