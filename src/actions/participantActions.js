import {
    GET_PARTICIPANTS_OF_EXPENSE_START,
    GET_PARTICIPANTS_OF_EXPENSE_DONE,
    GET_PARTICIPANTS_OF_EXPENSE_ERROR,
    CLEAR_PARTICIPANTS_LIST,
    DELETE_AUTH_STATE,
    ADD_PARTICIPANT_START,
    ADD_PARTICIPANT_DONE,
    ADD_PARTICIPANT_ERROR,
    DELETE_PARTICIPANT_START,
    DELETE_PARTICIPANT_DONE,
    DELETE_PARTICIPANT_ERROR,
    UPDATE_PARTICIPANT_START,
    UPDATE_PARTICIPANT_DONE,
    UPDATE_PARTICIPANT_ERROR,
    localURL,

} from "./actionConstants";
import axios from 'axios'
import {deleteState} from "../../src/localStorage"
import formUrlEncode from "form-urlencoded"
// import {signOut} from "./authActions";

/**
 * TODO ASAGIDAKI METODLARDAN HERHANGI BIRI CALISTIGINDA HERHANGI BISEY DISPATCH ETMESI SART YOKSA TYPEERROR
 * VERIYOR "CANNOT READ PROPERTY "TYPE" UNDEFINED
 */



export const loadParticipantsOfExpense = (isSignedIn, token, expenseid, amount) => {


    if (!isSignedIn) {
        return dispatch => ({
            type: GET_PARTICIPANTS_OF_EXPENSE_ERROR, payload: "invalid user"
        })
    }
    if (expenseid < 0 || amount === undefined) {
        return {type: GET_PARTICIPANTS_OF_EXPENSE_ERROR, payload: "hatali expense id"};
    }

    if (expenseid < 1) {
        return {type: GET_PARTICIPANTS_OF_EXPENSE_ERROR, payload: "hatali expense id"};
    } else {

        return async (dispatch) => {
            dispatch({
                type: GET_PARTICIPANTS_OF_EXPENSE_START
            });
            await axios
                .get(`${localURL}/api/${expenseid}/participants?token=${token}`)
                .then(({data}) => {
                    var amountOfJoints = null
                    if (data.length > 0) {
                        var joints = data.map((row) => {
                            return parseInt(row.isJoint)
                        })

                        amountOfJoints = amount / ((joints.filter((val) => {
                            return val === 1
                        }).length) + 1);


                        dispatch({
                            type: GET_PARTICIPANTS_OF_EXPENSE_DONE,
                            payload: {data: data, joints: joints, amountOfJoints: amountOfJoints}
                        });
                    } else {
                        dispatch({
                            type: GET_PARTICIPANTS_OF_EXPENSE_DONE,
                            payload: {data: data, joints: [], amountOfJoints: []}
                        });
                    }
                })
                .catch((error) => {

                    dispatch({
                        type: GET_PARTICIPANTS_OF_EXPENSE_ERROR,
                        payload: error
                    })
                    if (error.response.data.message === "Token has expired") {
                        dispatch({
                            type: CLEAR_PARTICIPANTS_LIST
                        })
                        deleteState();
                        dispatch({type: DELETE_AUTH_STATE})
                    }


                })
        }
    }

}

export const addParticipant = (isSignedIn, token, expenseId, form, amount) => {
    form.token = token;
    if (form.description.length === 0) {form.description = null}


    if (!isSignedIn) {
        return dispatch => ({
            type: ADD_PARTICIPANT_ERROR, payload: "invalid user"
        })
    }

    return async (dispatch) => {
        dispatch({type: ADD_PARTICIPANT_START})


        await axios
            .post(`${localURL}/api/${expenseId}/participant`, form)
            .then(({data}) => {
                if (data.success === true) {
                    dispatch({
                        type: ADD_PARTICIPANT_DONE,
                    })
                    dispatch(loadParticipantsOfExpense(isSignedIn, token, expenseId, amount ))
                }
            }).catch(error => {
                dispatch({
                    type: ADD_PARTICIPANT_ERROR,
                    payload: error
                })
                if (error.response.data.message === "Token has expired") {
                    dispatch({
                        type: CLEAR_PARTICIPANTS_LIST
                    })
                    deleteState();
                    dispatch({type: DELETE_AUTH_STATE})
                }
            })
    }
}

export const deleteParticipant = (isSignedIn, token, expenseId, id, amount) => {


    if (!isSignedIn) {
        return (dispatch) => ({
            type: DELETE_PARTICIPANT_ERROR, payload: "invalid user"
        })
    }

    return async (dispatch) => {
        dispatch({
            type: DELETE_PARTICIPANT_START
        });
        await axios
            .delete(`${localURL}/api/${expenseId}/participant/${id}?token=${token}`)
            .then(({data}) => {
                if (data.success === true) {
                    dispatch({type: DELETE_PARTICIPANT_DONE})
                    dispatch(loadParticipantsOfExpense(isSignedIn, token, expenseId, amount));
                }
            })
            .catch((error) => {
                dispatch({type: DELETE_PARTICIPANT_ERROR, payload: error})
                if (error.response.data.message === "Token has expired") {
                    dispatch({
                        type: CLEAR_PARTICIPANTS_LIST
                    })
                    deleteState();
                    dispatch({type: DELETE_AUTH_STATE})
                }
            })
    }

}

export const updateParticipant = (isSignedIn, token, expenseId, id, form, amount) => {

    if (form.amountOfCut === "") {
        form.amountOfCut = 0
    }

    const urlEncodedForm = formUrlEncode(form)
    if (!isSignedIn) {
        return dispatch => ({
            type: UPDATE_PARTICIPANT_ERROR, payload: "invalid user"
        })
    }
    return async (dispatch) => {
        dispatch({
            type: UPDATE_PARTICIPANT_START
        })

        await axios
            .put(`${localURL}/api/${expenseId}/participant/${id}?token=${token}`, urlEncodedForm)
            .then(({data}) => {
                if (data.success === true) {

                    dispatch({type: UPDATE_PARTICIPANT_DONE, payload: data.expense})
                    dispatch(loadParticipantsOfExpense(isSignedIn, token, expenseId, amount));
                }
            })
            .catch((error) => {
                dispatch({type: UPDATE_PARTICIPANT_ERROR, payload: error})
                if (error.response.data.message === "Token has expired") {
                    dispatch({
                        type: CLEAR_PARTICIPANTS_LIST
                    })
                    deleteState();
                    dispatch({type: DELETE_AUTH_STATE})
                }

            })
    }
}

export const clearParticipantsList = () => {

    return {type: CLEAR_PARTICIPANTS_LIST,}


}
