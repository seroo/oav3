import { createStore, applyMiddleware } from "redux";
import rootReducer from "../reducers/rootReducers";
import {composeWithDevTools} from "redux-devtools-extension";
import thunk from "redux-thunk";
// import logger from 'redux-logger';

export const configureStore = (preloadedState) => {
    // FIXME deactive logger in prod
    const middlewares = [thunk, /*logger*/];
    const middlewareEnhancer = applyMiddleware(...middlewares);

    const storeEnhancers = [middlewareEnhancer];

    const composedEnhancer = composeWithDevTools(...storeEnhancers);



    const store = createStore(
        rootReducer,
        preloadedState,
        composedEnhancer,
    )
    // console.log(store.getState());
    return store;
}
