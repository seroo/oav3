import {
    SIGN_IN_START,
    SIGN_IN_DONE,
    SIGN_IN_ERROR,
    SIGN_OUT_START,
    SIGN_OUT_DONE,
    SIGN_OUT_ERROR,
    REGISTER_ERROR,
    REGISTER_DONE,
    UPDATE_PROFILE_START,
    UPDATE_PROFILE_DONE,
    UPDATE_PROFILE_ERROR,
    DELETE_AUTH_STATE, REGISTER_START,
    GET_USER_DETAIL_ERROR,
    GET_USER_DETAIL_DONE,
    GET_USER_DETAIL_START, DELETE_USER_ACCOUNT_ERROR,
} from "../actions/actionConstants";

const initialState = {
    isSignedIn: false,
    user: null,
    error: null,
    token: null,
    updated: false,
    hasSigningOut: false,
    loading: false,
}

const authReducer = (state = initialState, action) => {
    switch (action.type) {
        case SIGN_IN_START:
            return {
                ...state,
                isSignedIn: false,
                user: null,
                token: null,
                error: null,
                loading: true,
            }
        case SIGN_IN_DONE:
            return {
                ...state,
                isSignedIn: true,
                user: action.payload.user,
                token: action.payload.token,
                error: null,
                loading: false,
            }
        case SIGN_IN_ERROR:
            return {
                ...state,
                isSignedIn: false,
                user: null,
                token: null,
                error: action.payload,
                loading: false,
            }
        case SIGN_OUT_START:
            return {
                ...state,
                loading: true,

            }
        case SIGN_OUT_DONE:
            return {
                isSignedIn: false,
                user: null,
                error: null,
                token: null,
                hasSigningOut: true,
                loading: false,
            }
        case SIGN_OUT_ERROR:
            return {
                ...state,
                error: action.payload,
                token: null,
                user: null,
                isSignedIn: false,
                hasSigningOut: false,
                loading: false,
            }
        case DELETE_AUTH_STATE:
            return {
                ...state,
                isSignedIn: false,
                user: null,
                error: null,
                token: null,
                updated: false,
                hasSigningOut: false,
                loading: false,
            }

        case REGISTER_DONE:
            return {
                ...state,
                isSignedIn: true,
                user: action.payload.user,
                error: null,
                token: action.payload.token,
                updated: false,
                loading: false,
            }

        case REGISTER_START:
            return {
                ...state,
                isSignedIn: false,
                user: null,
                error: null,
                token: null,
                updated: false,
                isSigning: true,
                loading: true,
            }
        case REGISTER_ERROR:
            return {
                isSignedIn: false,
                user: null,
                error: action.payload,
                token: null,
                updated: false,
                isSigning: false,
                hasSigningOut: false,
                loading: false,
            }
        case UPDATE_PROFILE_START:
            return {...state, error: null, updated: false, loading: true,}
        case UPDATE_PROFILE_DONE:
            return {
                ...state,
                updated: true,
                error: null,
                loading: false,
            }
        case UPDATE_PROFILE_ERROR:
            return {
                ...state,
                updated: false,
                error: action.payload,
                loading: false,
            }
        case GET_USER_DETAIL_START:
            return {
                ...state,
                loading: true,
            }
        case GET_USER_DETAIL_DONE:
            return {
                ...state,
                user: action.payload.user,
                loading: false,
            }
        case GET_USER_DETAIL_ERROR:
            return {
                ...state,
                error: action.payload,
                loading: false,
            }
        case DELETE_USER_ACCOUNT_ERROR:
            return {
                ...state,
                error: action.payload,
                loading: false,
            }

        default:
            return state
    }
}
export default authReducer;