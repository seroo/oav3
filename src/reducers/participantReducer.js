import {
    GET_PARTICIPANTS_OF_EXPENSE_DONE,
    GET_PARTICIPANTS_OF_EXPENSE_START,
    GET_PARTICIPANTS_OF_EXPENSE_ERROR,
    ADD_PARTICIPANT_START,
    ADD_PARTICIPANT_DONE,
    ADD_PARTICIPANT_ERROR,
    UPDATE_PARTICIPANT_ERROR,
    CLEAR_PARTICIPANTS_LIST, UPDATE_PARTICIPANT_START, UPDATE_PARTICIPANT_DONE,
} from "../actions/actionConstants";

const initialState = {
    loading: false,
    loaded: false,
    error: null,
    participants: [],
    joints: [],
    amountOfJoints: 0,
}

const participantReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_PARTICIPANTS_OF_EXPENSE_START:
            return {...state, loading: true, loaded: false, error: null};
        case GET_PARTICIPANTS_OF_EXPENSE_ERROR:
            return {...state, loading: false, loaded: false, error: action.payload};
        case GET_PARTICIPANTS_OF_EXPENSE_DONE:
            return {...state, loading: false,
                loaded: true,
                participants: action.payload.data,
                joints: action.payload.joints,
                error: null,
                amountOfJoints: action.payload.amountOfJoints,
            };
        case ADD_PARTICIPANT_START:
            return {...state, loaded: false, loading: true}
        case ADD_PARTICIPANT_DONE:
            return {...state, loaded: true, loading: false,}
        case ADD_PARTICIPANT_ERROR:
            return {...state, error: action.payload, loaded: false, loading: false,}
        case UPDATE_PARTICIPANT_START:
            return {...state, loaded: false, loading: true}
        case UPDATE_PARTICIPANT_DONE:
            return {...state, loaded: true, loading: false}
        case UPDATE_PARTICIPANT_ERROR:
            return {...state, error: action.payload, loaded: false, loading: false,}
        case CLEAR_PARTICIPANTS_LIST:
            return {...state, loading: false, loaded: false, error: null, participants: [], joints: [], amountOfJoints: 0}
        default:
            return state
    }

}

export default participantReducer;