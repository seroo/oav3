import { combineReducers } from "redux";
import participantReducer from "./participantReducer";
import expenseReducer from "./expenseReducer";
import authReducer from "./authReducer";

const rootReducer = combineReducers({
    getParticipant: participantReducer,
    getExpense: expenseReducer,
    auth: authReducer,
});
export default rootReducer;
