import {
    GET_EXPENSE_LIST_START,
    GET_EXPENSE_LIST_DONE,
    GET_EXPENSE_LIST_ERROR,
    GET_EXPENSE_BY_ID_START,
    GET_EXPENSE_BY_ID_DONE,
    GET_EXPENSE_BY_ID_ERROR,
    SAVE_EXPENSE_START,
    SAVE_EXPENSE_DONE,
    SAVE_EXPENSE_ERROR,
    DELETE_EXPENSE_ERROR,
    CHANGE_CLICKED_EXPENSE, DELETE_EXPENSE_START, DELETE_EXPENSE_DONE,
} from "../actions/actionConstants";

const initialState = {
    loading: false,
    loaded: false,
    error: null,
    saving: false,
    saved: true,
    expenseList: [],
    clickedExpense: {

        id: -1,
        description: '',
        place: '',
        variety: 'yemek',
        isJoint: 1,
        amount: 0,
    }
}

const expenseReducer = (state = initialState, action) => {
    // console.log(action)
    switch (action.type) {
        case GET_EXPENSE_LIST_START:
            return {...state, loading: true, loaded: false,};
        case GET_EXPENSE_LIST_ERROR:
            return {...state, loading: false, loaded: false, error: action.payload};
        case GET_EXPENSE_LIST_DONE:
            return {...state, loading: false, loaded: true, error: null, expenseList: action.payload.reverse(), clickedExpense: action.payload[0] || state.clickedExpense };
        case GET_EXPENSE_BY_ID_START:
            return {...state, loading: true, loaded: false,};
        case GET_EXPENSE_BY_ID_ERROR:
            return {...state, loading: false, loaded: false, error: action.payload};
        case GET_EXPENSE_BY_ID_DONE:
            return {...state, loading: false, loaded: true, error: null, clickedExpense: {
                    id: action.payload.id,
                    description: action.payload.description,
                    place: action.payload.place,
                    variety: action.payload.variety,
                    isJoint: action.payload.isJoint,
                    amount: action.payload.amount,
                }};
        case SAVE_EXPENSE_START:
            return {...state, saving: true, loading: true, loaded: false,}
        case SAVE_EXPENSE_DONE:
            return {...state, saving: false, loaded: true, loading: false, saved: true, error: null, clickedExpense: action.payload }
        case SAVE_EXPENSE_ERROR:
            return {...state, saving: false, loaded: false, loading: false, saved: false, error: action.payload }
        case DELETE_EXPENSE_START:
            return {...state, loading: true, }
        case DELETE_EXPENSE_DONE:
            return {...state, loaded: false, }
        case DELETE_EXPENSE_ERROR:
            return {...state, error: action.payload }
        case CHANGE_CLICKED_EXPENSE:
            return {...state, clickedExpense: action.payload }
        default:
            return state
    }
}

export default expenseReducer;