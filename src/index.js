import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import {Provider} from 'react-redux';
import {configureStore} from "./store/configureStore";
import {loadState, saveState} from "./localStorage";
import throttle from 'lodash/throttle';


const persistedState = loadState();
const store = configureStore(persistedState);

store.subscribe(throttle(() => {
    if (store.getState().auth.isSignedIn) {
        saveState(store.getState());
        // saveState({ böyle de sadece birine bağlanabilir.
        //     auth: store.getState().auth
        // });
    }
}, 1000));


let render = () => {
    ReactDOM.render(
        <Provider store={store}>
            <App/>
        </Provider>,
        document.getElementById("root")
    );
};
render();